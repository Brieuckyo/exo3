<?php

namespace App\Calculator;

use App\Contracts\ContractAdditionSoustraction;
use App\Contracts\InterfaceCalculate;
use App\Calculator\Exceptions\NoOperandsException;

class Division extends ContractAdditionSoustraction implements InterfaceCalculate
{

    protected $result;

    public function calculate()
    {
        if (count($this->operands) == 0) {
            throw new NoOperandsException();
        } else {
            foreach ($this->operands as $key => $operand) {
                if ($key == 0)
                    $this->total = $operand;
                else {
                    $this->total = $this->total / $operand;
                }
            }
            return $this->total;
        }
    }
}