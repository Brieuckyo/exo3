<?php

namespace App\Calculator;

use App\Calculator\Exceptions\NoOperandsException;
use App\Contracts\ContractAdditionSoustraction;
use App\Contracts\InterfaceCalculate;

class Addition extends ContractAdditionSoustraction implements InterfaceCalculate
{
    protected $result;

    public function calculate()
    {
        if (count($this->operands) == 0) {
            throw new NoOperandsException();
        } else {
            foreach ($this->operands as $operand) {
                $this->result += $operand;
            }
            return $this->result;
        }
    }
}


