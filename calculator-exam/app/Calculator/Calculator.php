<?php

namespace App\Calculator;

use App\Calculator\Addition;
use App\Contracts\ContractAdditionSoustraction;
use App\Contracts\InterfaceCalculate;

class Calculator extends ContractAdditionSoustraction implements InterfaceCalculate
{

    public $operations = [];

    public function setOperation(Addition $addition)
    {
        array_push($this->operations, $addition->operands);
    }

    public function getOperations()
    {
        return $this->operations;
    }

    public function setOperations()
    {
        foreach ($this->operations as $operation) {
            array_push($this->operations, $operation);
        }

    }

    public function calculate()
    {

    }
}