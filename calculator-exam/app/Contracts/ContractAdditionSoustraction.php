<?php

namespace App\Contracts;

use App\Calculator\Exceptions\NoOperandsException;

abstract class ContractAdditionSoustraction
{
    public $operands;

    public function setOperands(Array $array)
    {
        if (!isset($array) || !empty($array)) {
           $array = array_diff($array, [0]);
           $this->operands = $array;
        } else {
            throw new NoOperandsException();
        }
    }
}