<?php

namespace App\Contracts;

interface InterfaceCalculate
{
    public function calculate();
}